using Microsoft.VisualBasic.CompilerServices;
using System;

namespace Exampple_ESP_LR6
{
    delegate void MessageHandler(string message);
    delegate void HelloDelegate(string name);
    
    interface IHuman
    {
        string GetFullFio();
        string FirstName { get; }
        string LastName { get; }
        string MiddleName { get; }
    }
    
    class GeneralInfo : IHuman
    {
        public string FirstName { get; }
        public string LastName { get; }
        public string MiddleName { get; }
        public int Age { get; set; }

        public GeneralInfo(string firstname, string lastname, string middlename, int age) => 
            (FirstName, LastName, MiddleName, Age) = (firstname,lastname,middlename,age);
        public string GetFullFio() => $"{FirstName} {LastName} {MiddleName}";
    }
    
    interface ISpeciality
    {
        string TypeOfSport { get; }
        string Position { get; }

    }
    
    class WhiteJob : ISpeciality
    {
        public string TypeOfSport { get; }
        public string Position { get; }

        public WhiteJob(string typeofsport, string position) => (TypeOfSport, Position) = (typeofsport, position);

        public int Salary()
        {
            return 100 * ChekSpeciality() + ChekPosition() * 1000;
        }

        public int ChekSpeciality()
        {
            switch(Position)
            {
                case "Футбол": return 500;
                case "Хоккей": return 200;
                case "Волейбол": return 1000;
                default: return 2;
            }
        }

        public int ChekPosition()
        {
            switch (Position)
            {
                case "Работяга": return 500;
                case "Директор": return 200;
                case "Клерк": return 1000;
                default: return 2;
            }
        }
    }
    
    class BlackJob : ISpeciality
    {
        public string TypeOfSport { get; set; }
        public string Position { get; set; }

        public BlackJob(string title, string position) =>
            (title, position) = (TypeOfSport, Position);

        public int Salary()
        {
            return 10000 * ChekAuthority();
        }
        
        public int ChekAuthority()
        {
           Random rnd = new Random();
           return rnd.Next(1, 1000);
        }
    }
    
    class Human : IComparable<Human>
    {
        public GeneralInfo ginfo { get; set; }
        public event EventHandler LifeEnded;
        
        public void Worker()
        {
            int humanage = this.ginfo.Age;
            for (int i = humanage; i <= 100; i++)
            {
                System.Threading.Thread.Sleep(50);
                if (humanage >= 100)
                {
                    if (LifeEnded != null)
                    {
                        LifeEnded(this, new EventArgs());
                    }
                }
                humanage++;
            }
        }

        public void Show()
        {
            ShowMessage("hello!", delegate (string mes)
            {
                Console.WriteLine(mes);
            });
        }

        static void ShowMessage(string mes, MessageHandler handler)
        {
            handler(mes);
        }
        
        public ISpeciality Speciality { get; set; }

        public void Print()
        {
            WhiteJob whitejobobject = (WhiteJob)Speciality;
            Console.WriteLine($"Name: {this.ginfo.FirstName}, surname: {ginfo.LastName}, middle name: {ginfo.MiddleName}\nage: {ginfo.Age}" +
                $"\n profession: {Speciality.TypeOfSport}\n position: {Speciality.Position}");
        }

        public int CompareTo(Human obj)
        {
            return this.ginfo.Age.CompareTo(obj.ginfo.Age);
        }

        public static Human operator --(Human obj)
        {
            throw new System.ArgumentException("Age cannnot be degree");
        }

        public static void CopyObject(Human obj)
        {
            if(obj==null)
            {
                throw new System.ArgumentException("Parameter cannot be null");
            }
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            Human vlad = new Human();
            vlad.ginfo = new GeneralInfo("Аринович", "Владислав", "Игоревич", 20);
            vlad.Speciality = new WhiteJob("Футбол", "Тренер");
            vlad.LifeEnded += LifeEnded;
            vlad.Print();
            vlad.Show();
            vlad.Worker();

            Human katya = new Human();
            katya.ginfo = new GeneralInfo("Пульмановская", "Екатерина", "Дмитриевна", 20);
            katya.Speciality = new WhiteJob("Психолог", "Глава");
            katya.Print();
            Console.WriteLine(vlad.CompareTo(katya));

            var hi = new HelloDelegate(Hello);
            var best = new HelloDelegate(Best);
            var combinet = hi + best;
            combinet(vlad.ginfo.LastName);
            combinet -= best;
            combinet(vlad.ginfo.LastName);
            vlad--;
            HelloDelegate handler = delegate (string name)
            {
                Console.WriteLine($"{name} Анонимный метод");
            };
            handler("Hello");
        }
        
        static void Hello(string name)
        {
            Console.WriteLine($"Hello, {name}");
        }
        
        static void Best(string name)
        {
            Console.WriteLine($"Best, {name}");
        }
        
        private static void LifeEnded(object sender, EventArgs eventArgs)
        {
            if(sender is Human)
            {
                Console.WriteLine("Человек умер!");
            }
        }

    }
}
